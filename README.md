# Kaamelott Discord Bot

Just a bot replying a random quote of a Kaamelott character when mentioned.

The only character currently working are the ones having their own page.
So it works well with Kadoc, but not with Séfriane d'Aquitaine.

## Installation

```bash
pipenv install
```

## Usage

```sh
echo TOKEN={Discord app token} > .env
echo CHARACTER={Character name} >> .env
pipenv run python bot.py
```

## Deployment

### Heroku

```sh
heroku config:set -a {APPNAME} CHARACTER={Character name}
heroku config:set -a {APPNAME} TOKEN={Discord app token}

heroku git:remote -a {APPNAME}
git push heroku master
```
