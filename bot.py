import os
import random

import discord
import requests

from bs4 import BeautifulSoup
from dotenv import load_dotenv

load_dotenv()

TOKEN = os.getenv("TOKEN")
CHARACTER = os.getenv("CHARACTER")

# Load quotes from wikiquote
url = "https://fr.wikiquote.org/wiki/Kaamelott/{}".format(CHARACTER)
r = requests.get(url)
# Parse the page
soup = BeautifulSoup(r.text, "html.parser")
divs = soup.find_all("div", class_="citation")
quotes = [" ".join(tag.stripped_strings) for tag in divs]

client = discord.Client()


@client.event
async def on_message(message):
    # we do not want the bot to reply to itself
    if message.author == client.user:
        return

    if message.content.find("<@{}>".format(client.user.id)) != -1:
        msg = random.choice(quotes)
        await client.send_message(message.channel, msg)


@client.event
async def on_ready():
    print("Logged in as")
    print(client.user.name)
    print(client.user.id)
    print("------")


client.run(TOKEN)
